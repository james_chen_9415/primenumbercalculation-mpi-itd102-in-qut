from math import sqrt
from mpi4py import MPI
from time import time
import sys


def getallprimes(n):
    comm = MPI.COMM_WORLD  
    comm_rank = comm.Get_rank()  
    comm_size = comm.Get_size()
    comm_name = MPI.Get_processor_name()


    
    if comm_rank == 0:
       t1 = time()
       data = range(2,n+1)
       chunks = [[] for _ in range(comm_size)]
       for i, chunk in enumerate(data):
           chunks[i % comm_size].append(chunk)
       t3 = time()
       sys.stdout.write("Separating task costs %s seconds.\n" %(t3-t1))
    else:  
       data = None 
       chunks = None

       
    chunks = comm.scatter(chunks, root=0)

    primes = []
    for num in chunks:
        if is_prime(num):
            primes.append(num)
    sys.stdout.write("%d of %d on %s: Done!\n" %(comm_rank,comm_size, comm_name))
    
    combine_data = comm.gather(primes,root=0)  
    if comm_rank == 0:
        t2 = time()
        sys.stdout.write("Result: %s\n"%sorted(combine_data))
        sys.stdout.write("Whole task costs %s seconds.\n" %(t2-t1))

def is_prime(n):
    for i in range(2, int(sqrt(n) + 1)):
        if n % i == 0:
            return False
    return True

getallprimes(99)
