# README #
### Author ###
Email: james.chen.9415@gmail.com

### Summary ###
This repository is for QUT(Queensland University of Technology) ITD102 unit(2015) Raspberry Pi project.
In last assignment, we studied MPI and used 3 Raspberry Pi to make a server cluster for more powerful computing capability.

### Hardware ###
Router x 1
Raspberry Pi x 3 (power cable, SD card, tty, etc.)
Ethernet cable x 3

### Environment Setup ###
Ref:
http://www.southampton.ac.uk/~sjc/raspberrypi/pi_supercomputer_southampton.htm

### Coding ###
I coded and copied the py file to all of nodes (two slaves and one master)

### Run ###
Type the command in master node:
mpirun -np 3 -machinefile machinefile python prime.py

### Our Work ###
https://www.youtube.com/watch?v=u6zaxEzeajo